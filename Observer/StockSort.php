<?php
declare(strict_types=1);

namespace Prodovo\SaleableSorter\Observer;

use Magento\Catalog\Block\Product\ProductList\Toolbar as CoreToolbar;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class StockSort implements ObserverInterface
{

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var CoreToolbar
     */
    protected $coreToolbar;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        CoreToolbar $toolbar
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->coreToolbar = $toolbar;
    }
    /**
     * Execute observer
     *
     * @param Observer $observer
     * @return StockSort
     */
    public function execute(
        Observer $observer
    ) {
        /* Module Status */
        $enabled = $this->scopeConfig->getValue(
            'saleable_sorter/general/enabled',
            ScopeInterface::SCOPE_STORE
        );

        // If module enabled sort our collection.
        if ($enabled) {
            $collection = $observer->getEvent()->getData('collection');
            try {
                $websiteId = 0;
                $collection->getSelect()->joinLeft(
                    ['_inv' => $collection->getResource()->getTable('cataloginventory_stock_status')],
                    "_inv.product_id = e.entity_id and _inv.website_id=$websiteId",
                    ['stock_status']
                );
                $collection->addExpressionAttributeToSelect('in_stock', 'IFNULL(_inv.stock_status,0)', []);
                $collection->getSelect()->reset('order');
                $collection->getSelect()->order('in_stock DESC');

                if ($this->coreToolbar->getCurrentOrder() === 'price') {
                    $direction = $this->coreToolbar->getCurrentDirection();
                    $collection->getSelect()->order("min_price $direction");
                }
            } catch (\Exception $e) {
            }
            return $this;
        }
    }
}
